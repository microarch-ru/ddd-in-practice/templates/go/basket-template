# OpenApi (генерация HTTP сервера)
```
oapi-codegen -config configs/server.cfg.yaml https://gitlab.com/microarch-ru/microservices/dotnet/system-design/-/raw/main/services/basket/contracts/openapi.yml
```

# БД
```
https://pressly.github.io/goose/installation/
```

## Миграции
```
goose postgres "host=localhost user=username dbname=basket sslmode=disable password=secret" create init sql -dir="migrations"
goose postgres "host=localhost user=username dbname=basket sslmode=disable password=secret" up -dir="migrations"
goose postgres "host=localhost user=username dbname=basket sslmode=disable password=secret" down -dir="migrations"
```

## Запросы к БД
```
SELECT * FROM public.statuses;
SELECT * FROM public.delivery_periods;
SELECT * FROM public.goods;

SELECT b.*,s.name,t.name as delivery_period FROM public.baskets as b 
LEFT JOIN public.statuses as s on b.status_id=s.id 
LEFT JOIN public.delivery_periods as t on b.delivery_period_id=t.id;

SELECT * FROM public.items;
SELECT * FROM public.outbox;
```

## Очистка БД (все кроме справочников)
```
DELETE FROM public.baskets;
DELETE FROM public.items;
DELETE FROM public.outbox;
```

# gRPC Client
```
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
export PATH="$PATH:$(go env GOPATH)/bin"
protoc --proto_path=src --go_out=out --go_opt=paths=source_relative foo.proto bar/baz.proto
protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative helloworld/helloworld.proto
protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative https://gitlab.com/microarch-ru/ddd-in-practice/system-design/-/raw/main/services/discount/contracts/grpc.proto?ref_type=heads
```

# Документация используемых библилиотек
* [Goose] (https://github.com/pressly/goose)
* [Oapi-codegen] (https://github.com/oapi-codegen/oapi-codegen)
* [Protobuf] (https://protobuf.dev/reference/go/go-generated/)
* [gRPC] (https://grpc.io/docs/languages/go/)
* [Mockery] (https://vektra.github.io/mockery/latest/)